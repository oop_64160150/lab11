package com.anawin.week11;

public abstract class Animal {
    private String name;
    private int numberofleg;
    public Animal(String name,int numberofleg){
        this.name = name;
        this. numberofleg = numberofleg;
    }
    public String getName(){
        return name;
    }
    public int getNumberOfLeg(){
        return numberofleg;
    }
    public void setName(String name){
        this.name = name;
    }
    public void setNumberOfLeg(int numberofleg){
        this.numberofleg = numberofleg;
    }
    @Override
    public String toString() {
        return "Animal (" + name + ") has " + numberofleg + " legs";
    }
    public abstract void eat();

    public abstract void sleep();
}
