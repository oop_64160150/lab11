package com.anawin.week11;
class App 
{
    public static void main( String[] args )
    {
        Bat bat1 = new Bat("Batman");
        bat1.eat();
        bat1.sleep();
        bat1.takeoff();
        bat1.fly();
        bat1.landing();

        Fish fish1 = new Fish("Jukkui");
        fish1.eat();
        fish1.sleep();
        
        Plane plane1 = new Plane("Boeing","z8");
        plane1.takeoff();
        plane1.fly();
        plane1.landing();

        Bird bird1 = new Bird("Kukkuu");
        bird1.eat();
        bird1.sleep();
        bird1.takeoff();
        bird1.fly();
        bird1.landing();

        Submarine submarine1 = new Submarine("Dumnum", "z9");
        submarine1.swim();

        Crocodile crocodile1 = new Crocodile("Kaitong");
        crocodile1.eat();
        crocodile1.sleep();
        crocodile1.swim();
        crocodile1.Crawl();

        Snake snake1 = new Snake("Keaw");
        snake1.eat();
        snake1.sleep();
        snake1.Crawl();

        Rat rat1 = new Rat("Jerry");
        rat1.eat();
        rat1.walk();
        rat1.sleep();
        

        Dog dog1 = new Dog("Tongdang");
        dog1.eat();
        dog1.walk();
        dog1.sleep();
        

        Cat cat1 = new Cat("Garfield");
        cat1.eat();
        cat1.walk();
        cat1.sleep();
        

        Human human1 = new Human("zeus");
        human1.eat();
        human1.walk();
        human1.sleep();


        Flyable[] flyablesObjects = {bat1,plane1,bird1};
        for(int i = 0; i<flyablesObjects.length;i++){
            flyablesObjects[i].takeoff();
            flyablesObjects[i].fly();
            flyablesObjects[i].landing();
        }

        Swimable[] swimablesObjects = {fish1,submarine1,crocodile1};
        for(int i = 0; i<swimablesObjects.length;i++){
            swimablesObjects[i].swim();
        }

        Crawlable[] crawlablesObjects = {snake1,crocodile1};
        for(int i = 0; i<crawlablesObjects.length;i++){
           crawlablesObjects[i].Crawl();
        }

        Walkable[] walkablesObjects = {rat1,dog1,cat1,human1};
        for(int i = 0; i<walkablesObjects.length;i++){
            walkablesObjects[i].walk();
        }


    


    }
}
