package com.anawin.week11;

public class Submarine extends Vehicle implements Swimable {

    public Submarine(String name, String engineName) {
        super(name, engineName);    
    }

    @Override
    public void swim() {
        System.out.println(this.toString() + " swim.");
    }
    @Override
    public String toString() {
        return "Submarine("+this.getName()+")" + "engine: "+ this.getEngineName();    
    }
    
}
